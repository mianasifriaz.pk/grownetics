# Contracts, NDA's, MNDA's and More!

## Mutual Non Disclosure Agreements (MNDA)

These are often the first document exchanged with a customer or vendor. Standard practice is for us to recieve floorplans and requirements without and NDA but sometimes it's needed for client comfort. We also use MNDA's for vendors who are working on projects with us so that we can share confidential client site plans with them. So while clients may not even care and share things with us without them, it's our standard practice to have MNDA's with all vendors and channel partners. 

### Filling out an MNDA form

Go to All Company/Resources/NDA & Contract Templates to find the file Grownetics MNDA - FORM - Hellosign.pdf.

This is our e-signing template pdf. You can just upload this file to [Hello Sign](https://hellosign.com), or [docusign](https://docusign.com)

Follow instructions online to complete the form.

Send for signing, download signed copy and place in client or vendor folder.