# How to Hold a Productive Meeting

We try to systematize our weekly team meetings to allow everyone to schedule their work and focus during the week. Interruptions when you're focused on deep work are one of the most frustrating aspects of living in this connected world. To combat this we have developed a remote first and asynchronous work culture (see how-we-work.md on more about this). If you need to focus during the day let the team know in your standup no interruptions plz. Set your chat profile to do-not-disturb or away.

So you've run into an issue and you dont think you can move forward. Now you might think you need to call a meeting, but wait, review this handy 'Should I hold a meeting' infographic and then proceed.

## Regular Meetings at Grownetics

These are regularly scheduled meetings for our sprint workflow and company wide EOS implementation.

### Monday

- Client Project Review Meeting
    - Purpose: To review all client projects, to prioritize and remove blockers. Go project by project in wrike and update in both project folder and Ops Board as needed.

### Tuesday

- L10
    - Purpose: Review company wide projects and initiatives, ensure we're on track for quarterly rocks, IDS any issues.

### Friday

- Project/Sprint Review Meeting
    - Purpose to review the weeks work and sync before the weekend
- EOS Scorecard Meeting
    - Co-working to populate company scorecard

## Documenting your Meetings

For internal meetings do your best to just put the work where it should live. We got rid of our old internal meeting minutes doc for the weekly project review meetings and used wrike as the living meeting tool with comments for each task getting added to the task. 

We're tesing fireflies.ai so you can use that on internal meetings for text based searchable recordings from your meetings. 

For meetings with external parties just take notes in a doc under All Company/Meeting Notes/Client Name or by hand if in person (handwritten notes should be transcribed into a google doc with any tasks mentioned created in Wrike ASAP).

##### Should I hold a meeting?

Probably not, we have regularly scheduled meetings to cover all operations. If you need information from someone to get a task done add a comment and @ the person in the wrike task to bring them in to help. If you need to co-work on a big task to get it done with some other people schedule a co-working session for that specific task.

[Should I hold a meeting infographic link](https://hbr.org/resources/images/article_assets/2015/03/W150317_SAUNDERS_SHOULDHOLDMEETING.png)

Okay, so now you're either back at work or still stuck. In the case of still being stuck and you've exhausted your quick wins, get in touch with someone who can remove your blocker, if it's urgent CALL! If it's not as much put it in chat or propose a meeting date with the team members you'd like input from.

In preparation for the meeting make sure there is an agenda outlined in a fresh Meeting Doc Template placed in the correct Meetings folder. (Template docs are in All Company). Prep the doc and put the link in the google calendar events description then invite others.

During the meeting when one person is talking the other should be documenting what is being discussed. Prior to saying something type your topic/ question /idea so we end with an accurate record of what was discussed and their next actions.
