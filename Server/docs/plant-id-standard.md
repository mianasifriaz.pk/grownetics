# Grownetics Plant ID Standard

Using a consistent ID standard for tagging your plants can make your operation more efficient and increase traceability.

## Tagging Format

```
FACILITY / LOCATION ID - YEAR - CULTIVAR ID / SEED LOT ID - BATCH ID - PLANT ID
```

## Example Tag

```
123PINE-2020-BK13-12-42
```

This plant tag refers to the 42nd plant in the 12th batch of Bubba Kush varietal #13 grown in the year 2020 at the 123 Pine St. location.

## Physical Tagging

We use Metrc compatible tags.